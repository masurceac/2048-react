import React, {
    Component
} from 'react';
import {
    Unit
} from './Unit';
import './Game.css'

export class Game extends Component {

    

    constructor(props){
        super(props);
        this.getItems.bind(this);
        this.getItem.bind(this);
        let matrix = getRandomNumber(getMatrix());
        this.state = {
            matrix: matrix,
            oldMatrix: getMatrix(),
            animationName: ''
        }
        this.watchKeydown.bind(this);
        this.startAgagin.bind(this);
        this.watchKeydown()
    }    

    render() {

        return (
            <div>
                <div className="game-area">{this.getItems()}</div>
                <div className="game-menu">
                    <div className="game-menu__item">Current record: {this.getBestScore()}</div>
                </div>
            </div>
        );
    }


    startAgagin(){
        let matrix = getRandomNumber(getMatrix());
        this.setState({
            matrix: matrix,
            oldMatrix: getMatrix(),
            animationName: ''
        });
    }

    getBestScore() {

        return localStorage.getItem('bestScore') || 0;
    }


   

    getItems() {

        let result = [[],[],[],[]];

        
        for (let i=0; i<this.state.matrix.length; i++){
            for (let j=0; j<this.state.matrix[i].length; j++){
                result[i].push(
                    this.getItem(
                        this.state.matrix[i][j], 
                        this.getAnimation(this.state.matrix[i][j],this.state.oldMatrix[i][j])
                    )
                );
            }
        }
        return result;
    }

    getAnimation(current, old){

        if (!current || old == current){
            return '';
        }
        if (!old && current){
            return 'appear';
        }
        if (old == current / 2){
            return 'doublify';
        }
        return this.state.animationName;
    }

    getItem(val, animationName){

        return (<Unit key={Math.random().toString()} val={val} animationName={animationName}/>);
    }

    watchKeydown() {

        document.onkeydown = (e)=>{
        
            e = e || window.event;
            let newState;
            let oldMatrix;
            if (e.keyCode == '38') {
                oldMatrix = JSON.parse(JSON.stringify(this.state.matrix));
                newState = this.updateMatrix(this.state.matrix, 1,1, oldMatrix);
            }
            else if (e.keyCode == '40') {
                oldMatrix = JSON.parse(JSON.stringify(this.state.matrix));
                newState = this.updateMatrix(this.state.matrix, 1,-1, oldMatrix);
            }
            else if (e.keyCode == '37') {
                oldMatrix = JSON.parse(JSON.stringify(this.state.matrix));
                newState = this.updateMatrix(this.state.matrix,-1,-1, oldMatrix);
            }
            else if (e.keyCode == '39') {
                oldMatrix = JSON.parse(JSON.stringify(this.state.matrix));
                newState = this.updateMatrix(this.state.matrix,-1,1, oldMatrix);
            }
            if (newState){
                newState.oldMatrix = oldMatrix;
                this.setState(newState);
            }
        }
    }

    
    updateMatrix(matrix, r,c, oldMatrix){
        
        let animationName = '';

        if (r<0){
            if (c<0){
                for (let i=0; i<matrix.length; i++){
                    matrix = shiftLeft(matrix,i);
                    for (let j=0; j<matrix[i].length-1; j++){
                        if (matrix[i][j] == matrix[i][j+1]){
                            matrix[i][j]*=2;
                            matrix[i][j+1] = 0;
                        }
                    }
                    matrix = shiftLeft(matrix,i);
                    animationName = 'shiftLeft';
                }
            } else if (c>0){
                for (let i=0; i<matrix.length; i++){
                    matrix = shiftRight(matrix,i);
                    for (let j=matrix[i].length-1; j>0; j--){
                        if (matrix[i][j] == matrix[i][j-1]){
                            matrix[i][j]*=2;
                            matrix[i][j-1] = 0;
                        }
                    }
                    matrix = shiftRight(matrix,i);
                    animationName = 'shiftRight';
                }
            }
        } else if (r>0){
            if (c<0){
                for (let j=0; j<matrix.length; j++){
                    matrix = shiftDown(matrix,j);
                    for (let i=matrix.length-1; i>0; i--){
                        if (matrix[i][j] == matrix[i-1][j]){
                            matrix[i][j]*=2;
                            matrix[i-1][j] = 0;
                        }
                    }
                    matrix = shiftDown(matrix,j);
                    animationName = 'shiftDown';
                }
            } else if (c>0){
                for (let j=0; j<matrix.length; j++){
                    matrix = shiftUp(matrix,j);
                    for (let i=0; i<matrix.length-1; i++){
                        if (matrix[i][j] == matrix[i+1][j]){
                            matrix[i][j]*=2;
                            matrix[i+1][j] = 0;
                        }
                    }
                    matrix = shiftUp(matrix,j);
                    animationName = 'shiftUp';
                }
            }
        }
        let end =checkEnd(matrix);
        if (end){
            return {matrix:end, animationName: animationName};
        }
        if (shouldRender(matrix, oldMatrix)){
            matrix = getRandomNumber(matrix);
        }
        return {matrix:matrix ,animationName:animationName};
    }


}

function shouldRender(matrix,oldMatrix){
    for (let i=0; i<matrix.length; i++){
        for (let j=0; j<matrix.length; j++){
            if (matrix[i][j] != oldMatrix[i][j]){
                return true;
            }
        }
    }
    return false;
}

function shiftRight(matrix, row){

    matrix[row] = matrix[row].filter(c=>c!=0);
    while (matrix[row].length<4){
        matrix[row].unshift(0);
    }

    return matrix;
}


function shiftLeft(matrix, row){

    matrix[row] = matrix[row].filter(c=>c!=0);
    while (matrix[row].length<4){
        matrix[row].push(0);
    }

    return matrix;
}

function shiftUp(matrix, col) {
    let existing = [];
    for (let i=0; i<matrix.length; i++){
        if (matrix[i][col]){
            existing.push(matrix[i][col]);
        }
    }
    if (existing.length==matrix.length){
        return matrix;
    }
    
    for (let i=0; i<matrix.length; i++){
        if (existing.length){
            matrix[i][col] = existing.shift();
        } else {
            matrix[i][col] = 0;
        }
    }
    return matrix;

}

function shiftDown(matrix, col) {
    let existing = [];
    for (let i=matrix.length-1; i>=0; i--){
        if (matrix[i][col]){
            existing.push(matrix[i][col]);
        }
    }

    if (existing.length==matrix.length){
        return matrix;
    }
    
    for (let i=matrix.length-1; i>=0; i--){
        if (existing.length){
            matrix[i][col] = existing.shift();
        } else {
            matrix[i][col] = 0;
        }
    }
    return matrix;

}


function getMatrix() {
    
    return [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
}

function checkEnd(matrix){
    let available = getAvailablePositions(matrix);

    if (available.length ==0){
        alert('Cocker sucker!!! GAme over');
        setBestScore(matrix);
        return getRandomNumber(getMatrix());
    }

    return false;

}

function getAvailablePositions(matrix) {
    let available = [];

    for (let i=0; i<matrix.length; i++){
        for (let j=0; j<matrix.length; j++){
            if (!matrix[i][j]){
                available.push([i,j]);
            }
        }
    }
    return available;
}

function getRandomNumber(matrix){

    let available = getAvailablePositions(matrix);
    let index = Math.round(Math.random()*100) % available.length;
    
    let array = available[index];

    matrix[array[0]][array[1]] = [2,2,2,4][Math.round(Math.random()* 100) % 4];

    return matrix;

}

function setBestScore(matrix){
    let max = 0;
    for (let i=0; i<matrix.length; i++){
        for (let j=0; j<matrix[i].length; j++){
            if (matrix[i][j]>max) max=matrix[i][j];
        }

    }
    localStorage.setItem('bestScore', max);
}